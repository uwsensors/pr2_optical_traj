#include <stdlib.h>
#include <map>
#include <memory>
#include <set>
#include <vector>

#include <ros/ros.h>

// Dirty headers with warnings
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-register"
#pragma clang diagnostic ignored "-Wignored-qualifiers"
#include <eigen3/Eigen/Dense>

// MoveIt!
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/robot_model/robot_model.h>
#include <moveit/robot_state/robot_state.h>
#pragma clang diagnostic pop

#include <pr2_controllers_msgs/JointTrajectoryAction.h>
#include <actionlib/client/simple_action_client.h>

typedef actionlib::SimpleActionClient<pr2_controllers_msgs::JointTrajectoryAction> TrajClient;

struct JointState {
  double pos;
  double vel;
  double acc;
};

class RobotArm {
private:
  // Action client for the joint trajectory action 
  // used to trigger the arm movement action
  std::unique_ptr<TrajClient> traj_client_;

public:
  //! Initialize the action client and wait for action server to come up
  RobotArm() :
      // tell the action client that we want to spin a thread by default
      traj_client_(new TrajClient("r_arm_controller/joint_trajectory_action",
                                  true)) 
  {

    // wait for action server to come up
    while(!traj_client_->waitForServer(ros::Duration(5.0))){
      ROS_INFO("Waiting for the joint_trajectory_action server");
    }
    ROS_INFO("Found the action server");
  }

  //! Clean up the action client
  ~RobotArm() {
  }

  //! Sends the command to start a given trajectory
  void startTrajectory(pr2_controllers_msgs::JointTrajectoryGoal goal) {
    // When to start the trajectory: 0s from now
    goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.0);
    traj_client_->sendGoal(goal);
  }

  //! Generates a simple trajectory with two waypoints, used as an example
  /*! Note that this trajectory contains two waypoints, joined together
    as a single trajectory. Alternatively, each of these waypoints could
    be in its own trajectory - a trajectory can have one or more waypoints
    depending on the desired application.
    */
  pr2_controllers_msgs::JointTrajectoryGoal armExtensionTrajectory() {
    //our goal variable
    pr2_controllers_msgs::JointTrajectoryGoal goal;

    // First, the joint names, which apply to all waypoints
    goal.trajectory.joint_names.push_back("r_shoulder_lift_joint");
    goal.trajectory.joint_names.push_back("r_shoulder_pan_joint");
    goal.trajectory.joint_names.push_back("r_upper_arm_roll_joint");
    goal.trajectory.joint_names.push_back("r_elbow_flex_joint");
    goal.trajectory.joint_names.push_back("r_forearm_roll_joint");
    goal.trajectory.joint_names.push_back("r_wrist_flex_joint");
    goal.trajectory.joint_names.push_back("r_wrist_roll_joint");

    // We will have two waypoints in this goal trajectory
    goal.trajectory.points.resize(2);

    // First trajectory point
    int ind = 0;
    goal.trajectory.points[ind].positions.resize(7);
    goal.trajectory.points[ind].velocities.resize(7);
    for (int j = 0; j != 7; ++j) {
      // Positions
      goal.trajectory.points[ind].positions[j] = 0.0;
      // Velocities
      goal.trajectory.points[ind].velocities[j] = 0.0;
    }

    // To be reached 1 second after starting along the trajectory
    goal.trajectory.points[ind].time_from_start = ros::Duration(2.0);

    // Second trajectory point
    // Positions
    ind += 1;
    goal.trajectory.points[ind].positions.resize(7);
    goal.trajectory.points[ind].positions[0] = -0.067;
    goal.trajectory.points[ind].positions[1] = -0.250833;
    goal.trajectory.points[ind].positions[2] = -3.3;
    goal.trajectory.points[ind].positions[3] = 2.60;
    goal.trajectory.points[ind].positions[4] = -0.45;
    goal.trajectory.points[ind].positions[5] = -0.0;
    goal.trajectory.points[ind].positions[6] = 0.0;
    // Velocities
    goal.trajectory.points[ind].velocities.resize(7);
    for (size_t j = 0; j < 7; ++j)
    {
      goal.trajectory.points[ind].velocities[j] = 0.0;
    }
    // To be reached 2 seconds after starting along the trajectory
    goal.trajectory.points[ind].time_from_start = ros::Duration(5.0);

    //we are done; return the goal
    return goal;
  }

  //! Returns the current state of the action
  actionlib::SimpleClientGoalState getState()
  {
    return traj_client_->getState();
  }

};

class JointStateListener {
private:
  ros::NodeHandle n_;
  ros::Subscriber sub_;
  std::set<std::string> joints_;

public:
  JointStateListener (void) :
      n_{},
      sub_{n_.subscribe("joint_states", 10, &JointStateListener::Callback, 
                        this)},
      joints_{{
        "r_upper_arm_roll_joint",
          "r_shoulder_pan_joint",
          "r_shoulder_lift_joint",
          "r_forearm_roll_joint",
          "r_elbow_flex_joint",
          "r_wrist_flex_joint",
          "r_wrist_roll_joint"}}
  {
  }

  void Callback(const sensor_msgs::JointStateConstPtr &msg) {
    size_t num_joints = msg->name.size();
    for (size_t idx=0; idx != num_joints; ++idx) {
      if (joints_.count(msg->name[idx])) {
        ROS_INFO("%s: %2.3f %2.3f %2.3f",
                 msg->name[idx].c_str(),
                 msg->position[idx],
                 msg->velocity[idx],
                 msg->effort[idx]);
      } 
    }
    ROS_INFO("=====");
  }
};

class IKController {
private:
  robot_model_loader::RobotModelLoader robot_model_loader_;
  robot_model::RobotModelPtr kinematic_model_;

public:
  // BEGIN_TUTORIAL
  // Start
  // ^^^^^
  // Setting up to start using the RobotModel class is very easy. In
  // general, you will find that most higher-level components will
  // return a shared pointer to the RobotModel. You should always use
  // that when possible. In this example, we will start with such a
  // shared pointer and discuss only the basic API. You can have a
  // look at the actual code API for these classes to get more
  // information about how to use more features provided by these
  // classes.
  //
  // We will start by instantiating a
  // `RobotModelLoader`_
  // object, which will look up
  // the robot description on the ROS parameter server and construct a
  // :moveit_core:`RobotModel` for us to use.
  //
  // .. _RobotModelLoader: http://docs.ros.org/api/moveit_ros_planning/html/classrobot__model__loader_1_1RobotModelLoader.html
  IKController (void) :
      robot_model_loader_("robot_description"),
      kinematic_model_(robot_model_loader_.getModel())
      {
        ROS_INFO("Model frame: %s", kinematic_model_->getModelFrame().c_str());
      }

  int Calc(void) {
    // Using the :moveit_core:`RobotModel`, we can construct a
    // :moveit_core:`RobotState` that maintains the configuration
    // of the robot. We will set all joints in the state to their
    // default values. We can then get a
    // :moveit_core:`JointModelGroup`, which represents the robot
    // model for a particular group, e.g. the "right_arm" of the PR2
    // robot.
    robot_state::RobotStatePtr kinematic_state(new robot_state::RobotState(kinematic_model_));
    kinematic_state->setToDefaultValues();
    const robot_state::JointModelGroup* joint_model_group = kinematic_model_->getJointModelGroup("right_arm");

    const std::vector<std::string> &joint_names = joint_model_group->getJointModelNames();

    // Get Joint Values
    // ^^^^^^^^^^^^^^^^
    // We can retreive the current set of joint values stored in the state for
    // the right arm.
    std::vector<double> joint_values;
    kinematic_state->copyJointGroupPositions(joint_model_group, joint_values);
    for(std::size_t i = 0; i < joint_names.size(); ++i) {
      ROS_INFO("Joint %s: %f", joint_names[i].c_str(), joint_values[i]);
    }

    // Joint Limits
    // ^^^^^^^^^^^^
    // setJointGroupPositions() does not enforce joint limits by itself, but a
    // call to enforceBounds() will do it.
    /* Set one joint in the right arm outside its joint limit */
    joint_values[0] = 1.57;
    kinematic_state->setJointGroupPositions(joint_model_group, joint_values);

    /* Check whether any joint is outside its joint limits */
    ROS_INFO_STREAM("Current state is " << (kinematic_state->satisfiesBounds() ? "valid" : "not valid"));

    /* Enforce the joint limits for this state and check again*/
    kinematic_state->enforceBounds();
    ROS_INFO_STREAM("Current state is " << (kinematic_state->satisfiesBounds() ? "valid" : "not valid"));

    // Forward Kinematics
    // ^^^^^^^^^^^^^^^^^^
    // Now, we can compute forward kinematics for a set of random joint
    // values. Note that we would like to find the pose of the
    // "r_wrist_roll_link" which is the most distal link in the
    // "right_arm" of the robot.
    kinematic_state->setToRandomPositions(joint_model_group);
    const Eigen::Affine3d &end_effector_state = kinematic_state->getGlobalLinkTransform("r_wrist_roll_link");

    /* Print end-effector pose. Remember that this is in the model frame */
    ROS_INFO_STREAM("Translation: " << end_effector_state.translation());
    ROS_INFO_STREAM("Rotation: " << end_effector_state.rotation());

    // Inverse Kinematics
    // ^^^^^^^^^^^^^^^^^^
    // We can now solve inverse kinematics (IK) for the right arm of the
    // PR2 robot. To solve IK, we will need the following:
    //  * The desired pose of the end-effector (by default, this is the last
    //  link in the "right_arm" chain): end_effector_state that we computed in
    //  the step above.
    //  * The number of attempts to be made at solving IK: 5
    //  * The timeout for each attempt: 0.1 s
    double time_begin = ros::Time::now().toSec();
    bool found_ik = kinematic_state->setFromIK(joint_model_group, 
                                               end_effector_state, 10, 0.1);
    double time_end = ros::Time::now().toSec();
    ROS_INFO("IK computation took %f s", time_end - time_begin);

    // Now, we can print out the IK solution (if found):
    if (found_ik)
    {
      kinematic_state->copyJointGroupPositions(joint_model_group, joint_values);
      for(std::size_t i=0; i < joint_names.size(); ++i)
      {
        ROS_INFO("Joint %s: %f", joint_names[i].c_str(), joint_values[i]);
      }
    }
    else
    {
      ROS_INFO("Did not find IK solution");
    }

    // Get the Jacobian
    // ^^^^^^^^^^^^^^^^
    // We can also get the Jacobian from the :moveit_core:`RobotState`.
    Eigen::Vector3d reference_point_position(0.0,0.0,0.0);
    Eigen::MatrixXd jacobian;
    kinematic_state->getJacobian(joint_model_group, kinematic_state->getLinkModel(joint_model_group->getLinkModelNames().back()),
                                 reference_point_position,
                                 jacobian);
    ROS_INFO_STREAM("Jacobian: " << jacobian);
    // END_TUTORIAL

    ros::shutdown();
    return 0;
  }
};

int main(int argc, char** argv)
{
  // Init the ROS node
  ros::init(argc, argv, "robot_driver");
  ros::AsyncSpinner spinner{1};
  spinner.start();

  JointStateListener jsl;

  RobotArm arm;
  // Start the trajectory
  arm.startTrajectory(arm.armExtensionTrajectory());
  // Wait for trajectory completion
  while(!arm.getState().isDone() && ros::ok())
  {
    usleep(50000);
  }

  ros::shutdown();
}
